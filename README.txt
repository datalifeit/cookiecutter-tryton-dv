===================
cookiecutter-tryton-dv
===================

Cookiecutter template for a Tryton development environment


Quickstart
----------

Generate a development environment project::

    cookiecutter http://gitlab.com/datalifeit/cookiecutter-tryton-dv

.. _Cookiecutter: https://github.com/audreyr/cookiecutter

