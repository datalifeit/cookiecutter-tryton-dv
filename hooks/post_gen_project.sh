#!/bin/bash

echo "Start configuring environment!"
find . -name ".hgempty" -type f -delete

install_venv={{ cookiecutter.install_virtual_env }}
create_venv={{ cookiecutter.create_virtual_env }}
cpath=$PWD
if [ "$install_venv" = true ]; then
    pip install virtualenv
    echo "virtualenv installed!"
    pip install virtualenvwrapper
    echo "virtualenvwrapper installed!"
fi

if [ "$create_venv" = true ]; then
    if [ -f ~/.bashrc ]; then
        _bash_file=~/.bashrc
    else
        _bash_file=~/.bash_profile
    fi    
    if [ -z "$WORKON_HOME" ]; then
        echo export WORKON_HOME="\$HOME/{{ cookiecutter.virtual_env_path }}" >> "$_bash_file"
        echo export PROJECT_HOME="\$HOME/Devel" >> "$_bash_file"
        if [ -f /usr/local/bin/virtualenvwrapper.sh ]; then
            echo source /usr/local/bin/virtualenvwrapper.sh >> "$_bash_file"
        else
            echo source /usr/bin/virtualenvwrapper.sh >> "$_bash_file"
        fi
    fi
fi

hg clone http://{{ cookiecutter.repo_server }}/trytond -u {{ cookiecutter.version }}

hg clone http://{{ cookiecutter.repo_server }}/proteus -u {{ cookiecutter.version }}

hg clone http://{{ cookiecutter.repo_server }}/sao -u {{ cookiecutter.version }}

echo "Repositories cloned!"

if [ "$create_venv" = true ]; then
    source "$_bash_file"
    mkvirtualenv {{ cookiecutter.virtual_env_name }}
fi

pip install git+http://gitlab.com/datalifeit/tryton-dvconfig

cd trytond
python setup.py develop
cd ../proteus
python setup.py develop